package com.example.a20220511shamsidinbabadjanovnycschools.api

import com.example.a20220511shamsidinbabadjanovnycschools.model.School
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {

    @GET("/resource/s3k6-pzi2.json")
    fun getSchools(): Single<List<School>>

    @GET("/resource/f9bf-2cp4.json")
    fun getSchoolDetails(@Query("dbn") dbn: String): Single<List<School>>
}