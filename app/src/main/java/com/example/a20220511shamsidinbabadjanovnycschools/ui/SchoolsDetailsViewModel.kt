package com.example.a20220511shamsidinbabadjanovnycschools.ui

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.a20220511shamsidinbabadjanovnycschools.R
import com.example.a20220511shamsidinbabadjanovnycschools.data.SchoolRepository
import com.example.a20220511shamsidinbabadjanovnycschools.model.School
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import java.io.IOException
import javax.inject.Inject

@HiltViewModel
class SchoolsDetailsViewModel @Inject constructor(private val repository: SchoolRepository) :
    ViewModel() {

    private var dbn: String? = null
    private val subscriptions = CompositeDisposable()
    val schoolDetailsLiveData = MutableLiveData<School>()
    val progressLiveData = MutableLiveData<Boolean>()
    val errorLiveData = MutableLiveData<Int>()

    fun onViewCreated(dbn: String) {
        this.dbn = dbn
        loadDetails()
    }

    fun onErrorViewClicked() {
        loadDetails()
    }

    private fun loadDetails() {
        repository.getSchoolDetails(dbn.orEmpty())
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { progressLiveData.value = true }
            .doAfterTerminate { progressLiveData.value = false }
            .subscribe({ schools ->
                if (schools.isEmpty()) {
                    errorLiveData.value = R.string.school_details_not_found
                } else {
                    schoolDetailsLiveData.value = schools[0]
                }
            }, { throwable ->
                when (throwable) {
                    is IOException -> errorLiveData.value = R.string.network_error
                    else -> errorLiveData.value = R.string.unknown_error
                }
            })
            .also { subscriptions.add(it) }
    }

    override fun onCleared() {
        super.onCleared()
        subscriptions.dispose()
    }
}