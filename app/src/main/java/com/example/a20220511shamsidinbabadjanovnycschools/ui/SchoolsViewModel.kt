package com.example.a20220511shamsidinbabadjanovnycschools.ui

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.a20220511shamsidinbabadjanovnycschools.R
import com.example.a20220511shamsidinbabadjanovnycschools.data.SchoolRepository
import com.example.a20220511shamsidinbabadjanovnycschools.model.School
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import java.io.IOException
import javax.inject.Inject

@HiltViewModel
class SchoolsViewModel @Inject constructor(private val repository: SchoolRepository) : ViewModel() {

    private val subscriptions = CompositeDisposable()
    val schoolsLiveData = MutableLiveData<List<School>>()
    val progressLiveData = MutableLiveData<Boolean>()
    val errorLiveData = MutableLiveData<Int>()

    init {
        loadSchools()
    }

    private fun loadSchools() {
        repository.getSchools()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { progressLiveData.value = true }
            .doAfterTerminate { progressLiveData.value = false }
            .subscribe({ schools ->
                schoolsLiveData.value = schools
            }, { throwable ->
                when (throwable) {
                    is IOException -> errorLiveData.value = R.string.network_error
                    else -> errorLiveData.value = R.string.unknown_error
                }
            })
            .also { subscriptions.add(it) }
    }

    fun onErrorViewClicked() {
        loadSchools()
    }

    override fun onCleared() {
        super.onCleared()
        subscriptions.dispose()
    }
}