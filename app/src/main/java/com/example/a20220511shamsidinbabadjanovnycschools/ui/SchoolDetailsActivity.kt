package com.example.a20220511shamsidinbabadjanovnycschools.ui

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.example.a20220511shamsidinbabadjanovnycschools.R
import com.example.a20220511shamsidinbabadjanovnycschools.databinding.ActivitySchoolDetailsBinding
import com.example.a20220511shamsidinbabadjanovnycschools.model.School
import com.example.a20220511shamsidinbabadjanovnycschools.util.gone
import com.example.a20220511shamsidinbabadjanovnycschools.util.visible
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SchoolDetailsActivity : AppCompatActivity() {

    private lateinit var binding: ActivitySchoolDetailsBinding
    private val viewModel: SchoolsDetailsViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySchoolDetailsBinding.inflate(layoutInflater)
        setContentView(binding.root)
        title = getString(R.string.school_details)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        viewModel.errorLiveData.observe(this) { showError(it) }
        viewModel.progressLiveData.observe(this) { showProgress(it) }
        viewModel.schoolDetailsLiveData.observe(this) { showSchoolDetails(it) }

        binding.errorView.setOnClickListener {
            binding.errorView.gone()
            viewModel.onErrorViewClicked()
        }
        val dbn = intent.getStringExtra(SchoolsActivity.SCHOOL_DBN)
        viewModel.onViewCreated(dbn.orEmpty())
    }

    private fun showSchoolDetails(school: School) {
        binding.schoolName.text = school.schoolName
        val details = """
                Num of sat test takers: ${school.numOfSatTestTakers}
                Sat critical reading avg score: ${school.satCriticalReadingAvgScore}
                Sat math avg score: ${school.satMathAvgScore}
                Sat writing avg score: ${school.satWritingAvgScore}
            """.trimIndent()
        binding.satDetails.text = details
    }

    private fun showProgress(isProgress: Boolean) {
        if (isProgress) {
            binding.progressBar.visible()
        } else {
            binding.progressBar.gone()
        }
    }

    private fun showError(errorResId: Int) {
        val errorMsg = getString(errorResId)
        binding.errorView.text = errorMsg
        binding.errorView.visible()
    }

    override fun onSupportNavigateUp(): Boolean {
        if (!super.onSupportNavigateUp()) {
            supportFinishAfterTransition()
        }
        return true
    }
}