package com.example.a20220511shamsidinbabadjanovnycschools.ui

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.a20220511shamsidinbabadjanovnycschools.databinding.ItemSchoolBinding
import com.example.a20220511shamsidinbabadjanovnycschools.model.School

class SchoolsAdapter : RecyclerView.Adapter<SchoolsAdapter.ViewHolder>() {

    private var items = listOf<School>()
    var onItemClicked: ((String) -> Unit)? = null

    @SuppressLint("NotifyDataSetChanged")
    fun setData(items: List<School>) {
        this.items = items
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val itemBinding = ItemSchoolBinding.inflate(inflater, parent, false)
        return ViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position])
    }

    override fun getItemCount(): Int = items.size

    inner class ViewHolder(private val binding: ItemSchoolBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(school: School) {
            binding.schoolName.text = school.schoolName
            binding.overviewParagraph.text = school.overviewParagraph
            binding.showDetails.setOnClickListener {
                onItemClicked?.invoke(school.dbn.orEmpty())
            }
        }
    }
}