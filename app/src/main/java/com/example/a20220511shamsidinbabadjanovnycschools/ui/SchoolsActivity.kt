package com.example.a20220511shamsidinbabadjanovnycschools.ui

import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.a20220511shamsidinbabadjanovnycschools.databinding.ActivityMainBinding
import com.example.a20220511shamsidinbabadjanovnycschools.model.School
import com.example.a20220511shamsidinbabadjanovnycschools.util.gone
import com.example.a20220511shamsidinbabadjanovnycschools.util.visible
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SchoolsActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private val schoolsAdapter = SchoolsAdapter()
    private val viewModel: SchoolsViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        viewModel.progressLiveData.observe(this) { showProgress(it) }
        viewModel.schoolsLiveData.observe(this) { setData(it) }
        viewModel.errorLiveData.observe(this) { showError(it) }
        schoolsAdapter.onItemClicked = { openDetailsActivity(it) }

        binding.schoolsRecycler.apply {
            layoutManager = LinearLayoutManager(this@SchoolsActivity)
            adapter = schoolsAdapter
        }
        binding.errorView.setOnClickListener {
            binding.errorView.gone()
            viewModel.onErrorViewClicked()
        }
    }

    private fun openDetailsActivity(schoolDbn: String) {
        val intent = Intent(this, SchoolDetailsActivity::class.java)
        intent.putExtra(SCHOOL_DBN, schoolDbn)
        startActivity(intent)
    }

    private fun setData(schools: List<School>) {
        schoolsAdapter.setData(schools)
    }

    private fun showProgress(isProgress: Boolean) {
        if (isProgress) {
            binding.progressBar.visible()
        } else {
            binding.progressBar.gone()
        }
    }

    private fun showError(errorResId: Int) {
        val errorMsg = getString(errorResId)
        binding.errorView.text = errorMsg
        binding.errorView.visible()
    }

    companion object {
        const val SCHOOL_DBN = "schoolDBN"
    }
}