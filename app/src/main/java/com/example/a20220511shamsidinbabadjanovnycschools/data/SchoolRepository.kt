package com.example.a20220511shamsidinbabadjanovnycschools.data

import com.example.a20220511shamsidinbabadjanovnycschools.api.ApiService
import com.example.a20220511shamsidinbabadjanovnycschools.model.School
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SchoolRepository @Inject constructor(private val apiService: ApiService) {

    fun getSchools(): Single<List<School>> {
        return apiService.getSchools()
    }

    fun getSchoolDetails(dbn: String): Single<List<School>> {
        return apiService.getSchoolDetails(dbn)
    }
}