package com.example.a20220511shamsidinbabadjanovnycschools.di

import com.example.a20220511shamsidinbabadjanovnycschools.api.ApiService
import com.example.a20220511shamsidinbabadjanovnycschools.util.Const
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {

    @Provides
    fun provideApiService(gson: Gson): ApiService {
        return Retrofit.Builder()
            .baseUrl(Const.BASE_API_URL)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
            .create(ApiService::class.java)
    }
}