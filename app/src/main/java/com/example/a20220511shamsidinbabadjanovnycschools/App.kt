package com.example.a20220511shamsidinbabadjanovnycschools

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class App : Application()